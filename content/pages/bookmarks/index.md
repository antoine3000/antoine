---
title: "Bookmarks"
openView: true
date: 2019-11-01
---

- How to Build a Low-tech Website? [(link)](https://solar.lowtechmagazine.com/2018/09/how-to-build-a-lowtech-website.html)
- Jenny Odell: "How to Do Nothing: Resisting the Attention Economy" [(link)](https://www.youtube.com/watch?v=izjlP9qtmBU)
- My website is a shifting house next to a river of knowledge [(link)](https://thecreativeindependent.com/people/laurel-schwulst-my-website-is-a-shifting-house-next-to-a-river-of-knowledge-what-could-yours-be/)
- Death to Bullshit [(link)](http://deathtobullshit.com/)
- The Critical Engineering Manifesto [(link)](https://criticalengineering.org/en)
- This is your phone on feminism [(link)](https://conversationalist.org/2019/09/13/feminism-explains-our-toxic-relationships-with-our-smartphones/)
