---
title: "Netwerk Aalst"
categories: ["websites"]
tags: ["Variable"]
---

Front-end development of the Netwerk Aalst website, an international center for contemporary art and an independent cinema located in Aalst, Belgium.
{{< button title="netwerkaalst.be" url="https://netwerkaalst.be/" >}}

{{< device type="desktop" name="variable-netwerk-3.jpeg" >}}
{{< device type="mobile" name="variable-netwerk-6.jpeg" >}}
{{< device type="mobile" name="variable-netwerk-7.jpeg" >}}
{{< device type="mobile" name="variable-netwerk-5.jpeg" >}}
{{< device type="desktop" name="variable-netwerk-1.jpeg" >}}
{{< device type="desktop" name="variable-netwerk-2.jpeg" >}}

{{< credit-variable >}}
