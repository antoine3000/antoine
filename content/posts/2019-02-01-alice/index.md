---
title: "Alice Gallery"
categories: ["websites"]
tags: ["Variable"]
---

Design and front-end development of the Alice Gallery website, an art gallery founded by Alice van den Abeele and Raphaël Cruyt and located in Brussels.
{{< button title="alicebxl.com" url="https://alicebxl.com/" >}}

{{< device type="desktop" name="variable-alice-1.jpeg" >}}
{{< device type="desktop" name="variable-alice-2.jpeg" >}}
{{< device type="desktop" name="variable-alice-3.jpeg" >}}
{{< device type="desktop" name="variable-alice-4.jpeg" >}}
{{< device type="desktop" name="variable-alice-5.jpeg" >}}
{{< device type="mobile" name="variable-alice-6.jpeg" >}}
{{< device type="mobile" name="variable-alice-7.jpeg" >}}

{{< credit-variable >}}
