---
title: "Learning OpenSCAD"
categories: ["Experiment"]
tags: ["3D"]
image: "cover-openscad-bubbles.jpg"
---

I've started learning how to make 3D modeling by writing code. I use an open-source software, [OpenSCAD](https://www.openscad.org/), that seems to have all the things I'm looking for in a CAD tool. Make sure to always have this [cheatsheet](https://www.openscad.org/cheatsheet/index.html) close to you while writing models.

{{< img name="cover-openscad-bubbles.jpg" >}}
{{< img name="openscad-dice.jpg" >}}

## Weird cube
{{< img name="openscad-cube.jpg" >}}
{{< highlight openscad >}}
wallTickness = 1;
size = 50;
steps = 150;
totalRotation = 30;
layer = 2;
k = 2;
module makeCubeEdges(size, rotation, mustEtchBase) {
  d1 = size - wallTickness * 2;
  d2 = size + wallTickness;
  rotate([rotation, rotation, rotation])
  difference() {
    // full cube
    cube([size, size, size], true);
    // remove center
    cube([d1, d2, d1], true); // y axis
    cube([d1, d1, d2], true); // z axis
    cube([d2, d1, d1], true); // x axis
    // finetuning for 3D print
    if (mustEtchBase) {
      translate([0, 0, -size / 2])
      cube([d1, d2, layer * 2], true); // dig along Z axis
    }
  }
}
for (i=[0:steps]) {
  makeCubeEdges(size * (1 - i/steps), totalRotation * pow(i/steps*k, 2));
}
{{</ highlight >}}

## A printable box
{{< img name="openscad-box.jpg" >}}
{{< highlight openscad >}}
$fn = 10;
length = 50;
width= 50;
height = 20;
radius = 4;
wallTickness = 2;

module roundedBox(length, width, height, radius) {
  dRadius = 2*radius;
  minkowski() {
    cube(size = [width-dRadius, length-dRadius, height]);
    cylinder(r = radius, h = 0.01);
  }
}
// box
translate([radius, radius, 0]) {
  difference () {
    roundedBox(length, width, height, radius);
    translate([wallTickness, wallTickness, wallTickness]) {
      roundedBox(length-wallTickness*2, width-wallTickness*2, height, radius);
    }
  }
}
// lid
translate([width*2 + radius, 0, 0]){
  mirror([1,0,0]) {
    translate([radius, radius, 0]) {
      union() {
        roundedBox(length, width, 1, radius);
        difference() {
          translate([wallTickness, wallTickness, 0]) {
            roundedBox(length-wallTickness*2, width-wallTickness*2, 4, radius);
          }
          translate([wallTickness*2, wallTickness*2, 0]) {
            roundedBox(length-wallTickness*4, width-wallTickness*4, 6, radius);
          }
        }
      }
    }
  }
}
{{</ highlight >}}