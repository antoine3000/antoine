---
title: "Tattoo flash #01"
categories: ["drawings"]
tags: ["tattoo"]
image: "cover-scan-2.jpg"
---

Some illustrations I would like to tattoo.

{{< img name="scan-1.jpg" alt="tattoo flash #01 - 01" >}}
{{< img name="cover-scan-2.jpg" alt="tattoo flash #01 - 02" >}}

[send me an email](mailto:focus404@pm.me) if you are interested.

Follow this project on [instagram](https://www.instagram.com/focus.404/).
