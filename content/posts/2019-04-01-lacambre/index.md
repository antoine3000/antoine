---
title: "La Cambre"
categories: ["websites"]
tags: ["Variable"]
---

Front-end development of the La Cambre website, one of the leading schools of art and design in Belgium.
{{< button title="lacambre.be" url="http://lacambre.be" >}}

{{< device type="mobile" name="variable-lacambre-2.jpeg" >}}
{{< device type="mobile" name="variable-lacambre-3.jpeg" >}}
{{< device type="mobile" name="variable-lacambre-4.jpeg" >}}
{{< device type="desktop" name="variable-lacambre-9.jpeg" >}}
{{< device type="desktop" name="variable-lacambre-7.jpeg" >}}
{{< device type="desktop" name="variable-lacambre-8.jpeg" >}}



{{< credit-variable >}}
