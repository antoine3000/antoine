---
title: "The Word Radio"
categories: ["websites"]
tags: ["Variable"]
---

Front-end development of The Word Radio website, an online community radio run by The Word from studios located in Brussels (Belgium).
{{< button title="theword.radio" url="https://www.theword.radio/" >}}

{{< device type="desktop" name="variable-thewordradio-1.jpeg" >}}
{{< device type="mobile" name="variable-thewordradio-2.jpeg" >}}
{{< device type="mobile" name="variable-thewordradio-3.jpeg" >}}
{{< device type="mobile" name="variable-thewordradio-4.jpeg" >}}
{{< device type="desktop" name="variable-thewordradio-5.jpeg" >}}
{{< device type="desktop" name="variable-thewordradio-6.jpeg" >}}

{{< credit-variable >}}
